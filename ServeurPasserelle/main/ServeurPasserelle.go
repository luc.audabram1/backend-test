package main

import (
	"bufio"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net"
	"strings"
	"sync"
)

var wg2 sync.WaitGroup

const mdp = "$2a$10$y4MVuUBNIIAepbTlgPoatuzE5NW0R3djzU9CgEYTSDvFr/zr3iswO"

const serverKey = `-----BEGIN EC PARAMETERS-----
BggqhkjOPQMBBw==
-----END EC PARAMETERS-----
-----BEGIN EC PRIVATE KEY-----
MIIEowIBAAKCAQEAyGT88tBHxmEXXDqf97ghPY1B1idQvK/VfOOMfzE5IfCTNG79
peBZTwhOQjg9epRMqYhl49ZqCZ74UumEc9+hd7TBWGPx39t1zLARDDSlT4RTHZ5K
fhHLM2we7NMf0/2gL1rWEdnvr69pq28lQp9hnbzyPwwMWAAXFtM42iL0mKNFSu5J
GBffmsV0/QoaePGyZUwjm/8g4osQG8TpCl9y3RhX3g4jQSOLdpIqRLpHTAOSh02R
Zk5BPS1V5eN4B28wpH1nUQIVqLzdzCFzTtaEyQTfKj+Eo+Cl94jl3a2XQ4IUwyGc
fZrEAE6M8KuIKNp8Z8mdVJuITAqRh5Syh4QAzwIDAQABAoIBAGdFlA3WRIb5lj5T
gAj7fTXpTBp/HF6BBzZbVhZeWaEiB9qnsFQBsETIdPwvW4+GSZO81TV7VB4tLLAd
48KLZvz3zAQvTYEd6jFYMxDlOKQn/2VuI9mgdeXEPJOzod1wAVMmLtf4Rkt1zLEq
kDsrQ/U0YGsZXGslcMC0jcHs7bySzWgnQu/FFmbE80sK4aRlFdaMai2ETBK2Nd39
cfYhuSAcvn2PC5L25GyfEeEkWcE1IpjJpCx4R8PY0PydMQsJpC8EVqNqfZhV15FZ
4OtEO1qcPIhAQ8hrF7GMKRpZ6zTfXG1P5iyk0oiVtn/aRdAorc6bh5bSXc1Cf2Mb
LUYGwQECgYEA9kSCWmyHXBVj0+AEKMhrscuO2nAW6DYWR7cr1A1/BSyo9SdnymWt
B6SMv64WBrhoCglBlPEXLz3VfDwGqC0UNAs+6BUovDnfOCUTc8QVhZw5ayr+4T3m
0Uo/6GttRK+1gpqjWL9ld11xadcu+yHfbM+1j15oGMNTaLNN4iN53Q8CgYEA0FBh
U9Zj4fOBayUxYuRF1H0pYiPO7FKDHo79SYi9z0HwFgDvcCRwC+Gjrb5rK2rKi2dS
H08V1rUSabq724oMxOW0hFwN+czUR3jeG2WLJSx23n1QSF//EzxVBZVG/wL9iq3s
6xnLfd0mi/oVzT1yBLrjyO/8Xw7zjOaR6Wj/IEECgYA2Z2ActckZcKhDEEQa9lvE
RWHjo8uLtRcq9K48BFdBP2J79h6ZueiFvbZSShUXmvlw7iaMzs3+Y7yb227lDEBg
VvOotbPwESDc+GPZuUG/6IlOwYrZG6G4Lpz7rW/QMrj4h0z6uB2kRwJdDWqVlCAV
m7VSXNrGTbBJewMDqC6txQKBgEbekb4L+f8s8KKbDKowLvGTgzi4JfWQPLtwugsC
UlDohd12g/8K5nOkWCML3TnwIug6Rl4QccWR5KnDtZWC6cUywK8nAjoxzSZjoGyB
p00GS4p155I31nec1vAHURgA1HERqsBxDiHL0wlcE363oMqrKkN6IcnVNFaDOrDF
38YBAoGBANvEGD5FTbJsqzceTVFtbHn/WUmQh7+F1onnYkxJLU4OqabESblSj2j2
Gzx2iGCUeJ4vpYgu7l6IUJ0jnl30gFIHtZVxAVDxrU2Fmkx0R+Rkobq/6xsEaNzC
iezA7mwr51ONiLfhC6hPjFHdbk3ZQ0b08OKoHvtavLF5LnMkqo/F
-----END EC PRIVATE KEY-----
`

const serverCert = `-----BEGIN CERTIFICATE-----
MIICwzCCAaugAwIBAgIJAJtri+yD+WqIMA0GCSqGSIb3DQEBBQUAMBQxEjAQBgNV
BAMTCWxvY2FsaG9zdDAeFw0yMDA2MTMxMjQ5MzBaFw0zMDA2MTExMjQ5MzBaMBQx
EjAQBgNVBAMTCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAMhk/PLQR8ZhF1w6n/e4IT2NQdYnULyv1XzjjH8xOSHwkzRu/aXgWU8ITkI4
PXqUTKmIZePWagme+FLphHPfoXe0wVhj8d/bdcywEQw0pU+EUx2eSn4RyzNsHuzT
H9P9oC9a1hHZ76+vaatvJUKfYZ288j8MDFgAFxbTONoi9JijRUruSRgX35rFdP0K
GnjxsmVMI5v/IOKLEBvE6Qpfct0YV94OI0Eji3aSKkS6R0wDkodNkWZOQT0tVeXj
eAdvMKR9Z1ECFai83cwhc07WhMkE3yo/hKPgpfeI5d2tl0OCFMMhnH2axABOjPCr
iCjafGfJnVSbiEwKkYeUsoeEAM8CAwEAAaMYMBYwFAYDVR0RBA0wC4IJbG9jYWxo
b3N0MA0GCSqGSIb3DQEBBQUAA4IBAQChCkprDhKdnSIa1ALkYYhyrnuviXxq9gUv
bTtoHMG4ifNJU6c51gOKG98k9vumfEYX9esyt+YuMx2MzCqTvWs7zd6vPzeDW7u0
VaSHMZMbdRbkmm/dTNc8kBbA5He2B/MYazVD86oLk3Uz0rjfiy/xRFybShmzCRzu
wC8u6xBlyvMb2uZlEmwSfbh1+GWlWc/i4S4/XotO+oQ8knPIGu2KmJMoLTBgbCnB
fVYZGTZA2XfkC9KegD17bcth7kcUxKNfuA3uSnNfVha8D2wmR+1uE/PKUDc5BqWn
HT9mayHgdiVa3ugY2Vi/nCHzXllIn4uZAxRoUKDiJ9ETEQXbRDFW
-----END CERTIFICATE-----`

const rootCertServ = `-----BEGIN CERTIFICATE-----
MIICwzCCAaugAwIBAgIJAPqfFeNy18fcMA0GCSqGSIb3DQEBBQUAMBQxEjAQBgNV
BAMTCWxvY2FsaG9zdDAeFw0yMDA2MTMxMjQ3NTlaFw0zMDA2MTExMjQ3NTlaMBQx
EjAQBgNVBAMTCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBANULT+Nj1XLJ7db0VKfvHMA7V7rwvRxsKzyNtybO03eyKsCi+WzwJiOxcRzI
JVaOMdKxwLgQ4f2Z7aGqJuQKpMPhP8l6Cf/aZd6Gh24fKFs/zFndmmAgyXHu5X0r
bkj6B8vxYAD4BYoHS9rYldTmYa0GVjecHFDRWeYRvn8u1fKzo6i64hbqrRT8aPIx
NTNcGGVlzHtRj/tu1/gPZB3U4OuHaHGH/O9rytw/tkqsmShBcyA2pHQLpQNo2ovS
gVW8bg0LRFGDFrzKHGYoIGwgL2VpruxatUfTJzBhCWHhHU8NckX23DfTHCabYEKf
qZLKUwcArDXA42uvtj44k8biEH0CAwEAAaMYMBYwFAYDVR0RBA0wC4IJbG9jYWxo
b3N0MA0GCSqGSIb3DQEBBQUAA4IBAQBiETWA9v8AFh73BN17NTkAZI2GcnamzYlU
FYQUFdF33Cezbud0UX6j5CPjpHZvJFqJCed1IB2RFY6+Gh/HsAX0Fk++2XqWTR6W
9lATiVW5VGBhuziUyshsR+ru/kdNcyZJAMCrolDOJHB/WT7JT6Xe5BHFHlWRn+aG
djlG70pDx+Xxj5XHJgHE3Cp9EJilET4F8glVz7VDiXH19P+CGNl0TpmnGpjtdtfN
mx+q4FcyUIHrMokA17s0MnCc+QFuMmkp/YCZtiuyBDyFhBU5mgiqHD+6fzqlp4lb
kfW2mBWu3h5f4Yg6K8np+faxNB9lmHi0wHWDViKDsDh4c2eRTCfV
-----END CERTIFICATE-----
`


func finConnection(c net.Conn){
	c.Write([]byte("*"))
	c.Close()
}

func authentication(c net.Conn) {

	defer finConnection(c)

	c.Write([]byte("Mot de passe : *"))

	netData, err := bufio.NewReader(c).ReadString('*')

	if err != nil {
		fmt.Println(err)
		return
	}

	temp := strings.TrimSpace(netData)
	temp = strings.Trim(temp,"*")
	temp = strings.Trim(temp,"\n")

	if bcrypt.CompareHashAndPassword([]byte(mdp), []byte(temp)) == nil {
		relation(c)
	} else {
		c.Write([]byte("INVALIDE*"))
	}
}

func relation(c net.Conn)  {

	defer finConnection(c)

	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(rootCertServ))
	if !ok {
		log.Fatal("failed to parse root certificate")
	}
	config := &tls.Config{RootCAs: roots, ServerName: "localhost"}

	serv, err := tls.Dial("tcp", "localhost:6001", config)
	if err != nil {
		log.Fatal(err)
	}

	go lecturePasserelle(c, serv)
	go envoiePasserelle(c, serv)

	wg2.Add(1)
	wg2.Wait()
}

func lecturePasserelle(client net.Conn, server net.Conn) {
	defer wg2.Done()

	for {
		message, _ := bufio.NewReader(server).ReadString('*')

		if message == "" {
			finConnection(client)
			server.Close()
			break
		}else{
			client.Write([]byte(message))
		}
	}
}

func envoiePasserelle(client net.Conn, server net.Conn) {
	for {
		message, _ := bufio.NewReader(client).ReadString('*')
		server.Write([]byte(message))
	}
}

func main() {
	certificate, err := tls.X509KeyPair([]byte(serverCert), []byte(serverKey))
	if err != nil {
		log.Fatal(err)
	}
	config := &tls.Config{Certificates: []tls.Certificate{certificate}}

	server, err := tls.Listen("tcp", ":6000", config)
	if err != nil {
		log.Fatal(err)
	}
	defer server.Close()

	for {
		client, err := server.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go authentication(client)
	}
}