package main

import (
	"backendTest/ServeurPrincipale/DataBase"
	"bufio"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"strings"
)

const server2Key = `-----BEGIN EC PARAMETERS-----
BggqhkjOPQMBBw==
-----END EC PARAMETERS-----
-----BEGIN EC PRIVATE KEY-----
MIIEowIBAAKCAQEA1QtP42PVcsnt1vRUp+8cwDtXuvC9HGwrPI23Js7Td7IqwKL5
bPAmI7FxHMglVo4x0rHAuBDh/Zntoaom5Aqkw+E/yXoJ/9pl3oaHbh8oWz/MWd2a
YCDJce7lfStuSPoHy/FgAPgFigdL2tiV1OZhrQZWN5wcUNFZ5hG+fy7V8rOjqLri
FuqtFPxo8jE1M1wYZWXMe1GP+27X+A9kHdTg64docYf872vK3D+2SqyZKEFzIDak
dAulA2jai9KBVbxuDQtEUYMWvMocZiggbCAvZWmu7Fq1R9MnMGEJYeEdTw1yRfbc
N9McJptgQp+pkspTBwCsNcDja6+2PjiTxuIQfQIDAQABAoIBAGsC5prqb7Hb+irJ
yagc9E0ewb914xfewY0GBiRpKVFrXgwrEKyGqDGvlIiHhDHI9P/HJ22NlWaEJhfZ
VvWe3wpBH4A9W753Xkn7OxW2ctf64NOW+08jyOwAuqc35IqlhLsNQFLcQjQF3DVO
K1yoaBG4KCCAurxhnA1zhkNuLCte4091YBk9YrFZ4Hh9J603GkU98ZLbp4JBOrFF
jNvPqrXeYFK5euXPdtiHeywBJS1tvLcnIYn/dj1x0J28YrjuXfvAO4qV8r/olxov
LFVU0+Ypaa5dxlYQ2zbwhdFoOO9qVxV7fZ7cilgcaP68fOEfN8iuoeSfptda514z
EVo24GUCgYEA+TTSR523quXvoVol2Gd75Dhec6AOeAvAItGXp2ozJZUIRjJ/Wkq5
cZfhbo+0bp/e1MSJTpsk9/6dAFceQ5q8lY+bdEJvstR3cke7OZfmb8q6XxWEeoUJ
2F6BiyriKSSgp5pJ8TXSmhTPLceaDFprvpYe2e1KGVgUUfM+ygF5WJMCgYEA2toe
p5h9IOFdSPqtwOUM8mvovVUg35JbU56FoA6zP6FcpI0c63g0HN+geD7EDaVQqUEL
D+UqNOC2T388UEp4am0KuLr1pFScPU0/Hc5cWFSJcNRcPUQmUIpAgkBfzzi7XPtt
r2WeqxQd89KlR6Idoz2fdWuAXdlEuRa8iur97K8CgYAe7BL7l/JmXkmxt8HibFTP
KL9vkXi6JJsetusedusJyshQDYwH5CXGlpX+Zg55e5TmycxP2KhprUpF9Ilo0hZ8
aGldr7C0e/CmWYg9yT9qt7kQe1cI7OEf2/ovDo8pk8XrhhWOvKL5Q4PBpnbS+ZVM
/MuvNuZu5qL+2DP+VNh9yQKBgQDZ802CFN/Qhf/z9QpjRvj82DCBlazWWpgvD3j+
LIWgwd9UVOBPNHeZpa/W8JMpD9Djrk4I921wGbYWN51mTLPdMVjzHeYSUN0EQ9Cu
q1vV7+KshiCSNzN9FCIoVVnVMieN8yuJ+Mbqk2886jQFwdKIRo9uTbAaRo0lUxpT
srFNKwKBgDJ3czSMgVOAWS+R4AFdL96J/Ar7ZSdebqZJvwQslJx0VWXt2P8nb1PB
KRHO/5w63+pJjhmyhS+sQMzDT1Nt3c6/bCVkmjnXIvUHzdT0qy5l3SDUNAuNgkHv
m6VyhYcK+UqhNIBrdzzOQzzgQPKiM1IYjX7TQUhdGIHsXJNCVPVd
-----END EC PRIVATE KEY-----
`
const server2Cert = `-----BEGIN CERTIFICATE-----
MIICwzCCAaugAwIBAgIJAPqfFeNy18fcMA0GCSqGSIb3DQEBBQUAMBQxEjAQBgNV
BAMTCWxvY2FsaG9zdDAeFw0yMDA2MTMxMjQ3NTlaFw0zMDA2MTExMjQ3NTlaMBQx
EjAQBgNVBAMTCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBANULT+Nj1XLJ7db0VKfvHMA7V7rwvRxsKzyNtybO03eyKsCi+WzwJiOxcRzI
JVaOMdKxwLgQ4f2Z7aGqJuQKpMPhP8l6Cf/aZd6Gh24fKFs/zFndmmAgyXHu5X0r
bkj6B8vxYAD4BYoHS9rYldTmYa0GVjecHFDRWeYRvn8u1fKzo6i64hbqrRT8aPIx
NTNcGGVlzHtRj/tu1/gPZB3U4OuHaHGH/O9rytw/tkqsmShBcyA2pHQLpQNo2ovS
gVW8bg0LRFGDFrzKHGYoIGwgL2VpruxatUfTJzBhCWHhHU8NckX23DfTHCabYEKf
qZLKUwcArDXA42uvtj44k8biEH0CAwEAAaMYMBYwFAYDVR0RBA0wC4IJbG9jYWxo
b3N0MA0GCSqGSIb3DQEBBQUAA4IBAQBiETWA9v8AFh73BN17NTkAZI2GcnamzYlU
FYQUFdF33Cezbud0UX6j5CPjpHZvJFqJCed1IB2RFY6+Gh/HsAX0Fk++2XqWTR6W
9lATiVW5VGBhuziUyshsR+ru/kdNcyZJAMCrolDOJHB/WT7JT6Xe5BHFHlWRn+aG
djlG70pDx+Xxj5XHJgHE3Cp9EJilET4F8glVz7VDiXH19P+CGNl0TpmnGpjtdtfN
mx+q4FcyUIHrMokA17s0MnCc+QFuMmkp/YCZtiuyBDyFhBU5mgiqHD+6fzqlp4lb
kfW2mBWu3h5f4Yg6K8np+faxNB9lmHi0wHWDViKDsDh4c2eRTCfV
-----END CERTIFICATE-----`

func finConnection1(c net.Conn){
	c.Write([]byte("*"))
	c.Close()
}

func Traitement(c net.Conn) {
	defer finConnection1(c)

	c.Write([]byte("Connexion succes*"))

	for {
		reçu, err := bufio.NewReader(c).ReadString('*')
		if err != nil {
			fmt.Println(err)
			return
		}

		reçu = strings.TrimSpace(reçu)
		reçu = strings.Trim(reçu,"*")
		reçu = strings.Trim(reçu,"\n")
		if strings.ToUpper(reçu) == "STOP" {
			break
		}

		go traitementDemande(c,reçu)
	}
	c.Close()
}

func traitementDemande(c net.Conn, reçu string) {
	reponse, err := DataBase.TraitementRequete(reçu)
	if err != nil {
		fmt.Fprintf(c, err.Error()+"*")
	} else {
		fmt.Fprintf(c, reponse+"*")
	}
}

func main() {
	DataBase.SetConnection()

	certificate, err := tls.X509KeyPair([]byte(server2Cert), []byte(server2Key))
	if err != nil {
		log.Fatal(err)
	}
	config := &tls.Config{Certificates: []tls.Certificate{certificate}}

	server, err := tls.Listen("tcp", ":6001", config)
	if err != nil {
		log.Fatal(err)
	}
	defer server.Close()

	for {
		client, err := server.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go Traitement(client)
	}
}