package DataBase

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"path"
	"strconv"
	"strings"
)

type Patient struct {
	numPat int
	nom string
	prenom string
}

type Maladie struct {
	numMaladie int
	intitule string
	date string
}

type Medicament struct {
	numMedicament int
	intitule string
}

func (p Patient) toString() string {
	return strconv.Itoa(p.numPat)+" "+p.nom+" "+p.prenom
}

func (m Maladie) toString() string {
	return strconv.Itoa(m.numMaladie)+" "+m.intitule+" "+m.date
}

func (m Medicament) toString() string {
	return strconv.Itoa(m.numMedicament)+" "+m.intitule
}

var DB *sql.DB

func SetConnection()  {
	if DB == nil{
		var err error
		DB, err = sql.Open("mysql", DB_USER+":"+DB_PASSWORD+"@/"+DB_NAME)
		if err != nil {
			panic(err)
		}
	}
}


func executeRequete(requete string)(*sql.Rows, error) {
	fmt.Println("REQUETE : '"+requete+"'")
	stmt, err := DB.Prepare(requete)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	rows, err := stmt.Query()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return rows, nil
}

func TraitementRequete(requete string) (string, error) {

	constant := strings.Split(requete, " ")
	if len(constant) != 2{
		return "", errors.New("Nombre d'argument invalide")
	}
	tab := strings.Split(constant[1], "/")

	switch strings.ToUpper(constant[0]) {
	case "POST":
		return postRequete(tab)
	case "GET":
		return getRequete(tab)
	case "PUT":
		return putRequete(tab)
	case "DELETE":
		return deleteRequete(tab)
	default:
		return "", errors.New("Argument 1 : Invalide (STOP, POST, GET, PUT, DELETE)")
	}
}

func postRequete(tab []string) (string, error){

	sqlRequete := "INSERT INTO";

	switch strings.ToUpper(tab[0]) {
	case "PATIENT":
		sqlRequete += " BEP_PATIENT (NOM, PRENOM) VALUES (";
		if len(tab) != 3{return "", errors.New("Erreur : NOM et PRENOM attendu")} else {
			sqlRequete += "\""+tab[1] + "\",\"" +tab[2] + "\")"
		}
		break;
	case "MALADIE":
		sqlRequete += " BEP_MALADIE (MA_INTITULE) VALUES (";
		if len(tab) != 2{return "", errors.New("Erreur : LIBELLE attendu")}else {
			sqlRequete += "\"" + tab[1] + "\")"
		}
		break;
	case "MEDICAMENT":
		sqlRequete += " BEP_MEDICAMENT (ME_INTITULE) VALUES (";
		if len(tab) != 2{return "", errors.New("Erreur : LIBELLE attendu")}else {
			sqlRequete += "\"" + tab[1] + "\")"
		}
		break;
	case "MALADIEPATIENT":
		sqlRequete += " BEP_HISTOMALADIE (NUMPAT, NUMMALADIE, DATE) VALUES (";
		if len(tab) != 3{return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MALADIE attendu")}else {
			bl1, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Argument 3, numero invalide.")
			}
			bl2, err := path.Match("[1-9]*", tab[2])
			if err != nil {
				return "", errors.New("Argument 3, numero invalide.")
			}
			if bl1 && bl2 {
				sqlRequete += tab[1] + "," +tab[2] + ", sysdate())"
			} else {
				return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MALADIE attendu")
			}
		}
		break;
	case "PRESCRIPTION":
		sqlRequete += " BEP_PRESCRIPTION (NUMPAT, NUMMEDICAMENT) VALUES (";
		if len(tab) != 3{return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MEDICAMENT attendu")}else {
			bl1, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Argument 3, numero invalide.")
			}
			bl2, err := path.Match("[1-9]*", tab[2])
			if err != nil {
				return "", errors.New("Argument 3, numero invalide.")
			}
			if bl1 && bl2 {
				sqlRequete += tab[1] + "," +tab[2] + ")"
			} else {
				return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MALADIE attendu")
			}
		}
		break;
	default:
		return "", errors.New("Argument 2, Données invalide (PATIENT, MALADIE, MEDICAMENT, MALADIEPATIENT, PRESCRIPTION)")
	}

	_, err := executeRequete(sqlRequete)
	if err != nil {
		return "", errors.New("Une erreur serveur est survenue lors de l'éxécution de la requete : "+err.Error())
	} else {
		return "SUCCES", nil;
	}
}

func getRequete(tab []string) (string, error){
	if len(tab) > 3{
		return "", errors.New("Nombre d'argument invalide")
	}

	var columnNum string

	patient := false
	maladie := false
	medicament := false

	sqlRequete := "SELECT * FROM";

	switch strings.ToUpper(tab[0]) {
	case "PATIENT":
		sqlRequete += " BEP_PATIENT";
		patient = true
		columnNum = "NUMPAT";
		break;
	case "MALADIE":
		sqlRequete += " BEP_MALADIE";
		maladie = true
		columnNum = "NUMMALADIE";
		break;
	case "MEDICAMENT":
		sqlRequete += " BEP_MEDICAMENT";
		medicament = true
		columnNum = "NUMMEDICAMENT";
		break;
	default:
		return "", errors.New("Argument 2, Données invalide (PATIENT, MALADIE, MEDICAMENT)")
	}

	if len(tab) == 3 {
		switch strings.ToUpper(tab[2]) {
		case "ALL":
			if !patient{return "", errors.New("Argument 4, invalide")}
			sqlRequete = "select * from ( SELECT NUMPAT, NUMMALADIE as \"num\", NOM, PRENOM, MA_INTITULE as \"initule\", DATE as \"date\" FROM BEP_PATIENT JOIN BEP_HISTOMALADIE USING (NUMPAT) JOIN BEP_MALADIE USING (NUMMALADIE) union SELECT NUMPAT, NUMMEDICAMENT as \"num\", NOM, PRENOM,ME_INTITULE as \"initule\", \"\" as \"date\" FROM BEP_PATIENT JOIN BEP_PRESCRIPTION USING (NUMPAT) JOIN BEP_MEDICAMENT USING (NUMMEDICAMENT) ) as ALLL"; maladie = true; patient = true; break;
		case "MALADIE":
			if maladie || medicament {return "", errors.New("Argument 4, invalide")}
			sqlRequete += " JOIN BEP_HISTOMALADIE USING (NUMPAT) JOIN BEP_MALADIE USING (NUMMALADIE)"; maladie = true; break;
		case "MEDICAMENT":
			if maladie || medicament{return "", errors.New("Argument 4, invalide")}
			sqlRequete += " JOIN BEP_PRESCRIPTION USING (NUMPAT) JOIN BEP_MEDICAMENT USING (NUMMEDICAMENT)"; medicament = true; break;
		case "PATIENT":
			if patient {return "", errors.New("Argument 4, invalide")}
			if maladie {sqlRequete += " JOIN BEP_HISTOMALADIE USING (NUMMALADIE) JOIN BEP_PATIENT USING (NUMPAT)"}
			if medicament {sqlRequete += " JOIN BEP_PRESCRIPTION USING (NUMMEDICAMENT) JOIN BEP_PATIENT USING (NUMPAT)"}
			patient = true;
			break;
		default:
			return "", errors.New("Argument 4, invalide")
		}
	}

	if len(tab) >= 2 {
		if (tab[1] != "") {
			bl, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Argument 3, numero invalide.")
			}

			if bl {
				sqlRequete += " WHERE " + columnNum + " = " + tab[1]
			} else {
				return "", errors.New("Argument 3, numero invalide.")
			}
		}
	}

	rows, err := executeRequete(sqlRequete)
	if err != nil {
		fmt.Println(err)
		return "", errors.New("Une erreur serveur est survenue (Requete non executable)")
	}

	reponse := ""

	nbInt := 0
	nbString := 0

	if patient{
		nbInt += 1
		nbString += 2
	}
	if maladie {
		nbInt += 1
		nbString += 2
	}
	if medicament {
		nbInt += 1
		nbString += 1
	}

	colsI := make([]int, nbInt)
	colsS := make([]string, nbString)

	cols := make([]interface{}, nbInt+nbString)
	for i := range colsI{
		cols[i] = &colsI[i]
	}
	for i := range colsS {
		cols[i+nbInt] = &colsS[i]
	}

	columns, _ := rows.Columns()

	for rows.Next(){
		var p Patient
		var ma Maladie
		ma.numMaladie = -1
		var me Medicament

		err := rows.Scan(cols...)
		if err != nil {
			fmt.Println(err)
			return "", errors.New("Une erreur serveur est survenue (code 2)")
		}

		indiceInt := 0
		indiceString := 0
		for _, str := range columns {
			str = strings.ToUpper(str)

			switch str {
			case "NUMPAT":
				p.numPat = colsI[indiceInt]
				indiceInt++
				break
			case "NUMMALADIE":
				ma.numMaladie = colsI[indiceInt]
				indiceInt++
				break
			case "NUMMEDICAMENT":
				me.numMedicament = colsI[indiceInt]
				indiceInt++
				break
			case "NUM":
				if ma.numMaladie == -1{
					ma.numMaladie = colsI[indiceInt]
				} else {
					me.numMedicament = colsI[indiceInt]
				}
				indiceInt++
				break
			case "DATE":
				ma.date = colsS[indiceString]
				indiceString++
				break
			case "MA_INTITULE":
				ma.intitule = colsS[indiceString]
				indiceString++
				break
			case "ME_INTITULE":
				me.intitule = colsS[indiceString]
				indiceString++
				break
			case "NOM":
				p.nom = colsS[indiceString]
				indiceString++
				break
			case "PRENOM":
				p.prenom = colsS[indiceString]
				indiceString++
				break
			default:
				break
			}
		}

		if patient{
			reponse += p.toString()+" "
		}
		if maladie{
			reponse += ma.toString()+" "
		}
		if medicament{
			reponse += me.toString()+" "
		}
		reponse += "\n"
	}

	if reponse != "" {
		return reponse, nil
	} else {
		return "", errors.New("L'ID Demandé n'existe pas")
	}
}

func putRequete(tab []string) (string, error) {
	if strings.ToUpper(tab[0]) != "PRESCRIPTION" {
		return "", errors.New("Seul les prescriptions peuvent être mise à jours")
	}

	if len(tab) != 4{
		return "", errors.New("Nombre d'argument invalide")
	}


	bl1, err := path.Match("[1-9]*", tab[1])
	if err != nil {
		return "", errors.New("Erreur : id invalide.")
	}
	bl2, err := path.Match("[1-9]*", tab[2])
	if err != nil {
		return "", errors.New("Erreur : id invalide.")
	}
	bl3, err := path.Match("[1-9]*", tab[3])
	if err != nil {
		return "", errors.New("Erreur : id invalide.")
	}

	if !bl1 || !bl2 || !bl3 {
		return "", errors.New("Erreur : id invalide.")
	}

	_, err = executeRequete("UPDATE BEP_PRESCRIPTION SET NUMMEDICAMENT=" + tab[3] + " WHERE NUMPAT = " + tab[1] + " and NUMMEDICAMENT = " + tab[2])
	if err != nil {
		return "", errors.New("Une erreur serveur est survenue lors de l'éxécution de la requete : "+err.Error())
	} else {
		return "SUCCES", nil;
	}
}

func deleteRequete(tab []string) (string, error){

	sqlRequete := "DELETE FROM"

	switch strings.ToUpper(tab[0]) {
	case "PATIENT":
		sqlRequete += " BEP_PATIENT WHERE NUMPAT = ";
		if len(tab) != 2{return "", errors.New("Erreur : ID attendu")} else {
			bl1, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Erreur, ID invalide.")
			}
			if bl1 {
				sqlRequete += tab[1]
			} else {
				return "", errors.New("Erreur, ID invalide.")
			}
		}
		break;
	case "MALADIE":
		sqlRequete += " BEP_MALADIE WHERE NUMMALADIE = ";
		if len(tab) != 2{return "", errors.New("Erreur : NUMERO MALADIE attendu")}else {
			bl1, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Erreur, NUMERO MALADIE invalide.")
			}
			if bl1 {
				sqlRequete += tab[1]
			} else {
				return "", errors.New("Erreur, NUMERO MALADIE invalide.")
			}
		}
		break;
	case "MEDICAMENT":
		sqlRequete += " BEP_MEDICAMENT WHERE NUMMEDICAMENT = ";
		if len(tab) != 2{return "", errors.New("Erreur : NUMERO MEDICAMENT attendu")}else {
			bl1, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Erreur, NUMERO MEDICAMENT invalide.")
			}
			if bl1 {
				sqlRequete += tab[1]
			} else {
				return "", errors.New("Erreur, NUMERO MEDICAMENT invalide.")
			}
		}
		break;
	case "MALADIEPATIENT":
		sqlRequete += " BEP_HISTOMALADIE WHERE NUMPAT = ";
		if len(tab) != 3{return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MALADIE attendu")}else {
			bl1, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Erreur, NUMERO PATIENT invalide.")
			}
			bl2, err := path.Match("[1-9]*", tab[2])
			if err != nil {
				return "", errors.New("Erreur, NUMERO MALADIE invalide.")
			}
			if bl1 && bl2 {
				sqlRequete += tab[1] + " and NUMMALADIE = " +tab[2]
			} else {
				return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MALADIE attendu")
			}
		}
		break;
	case "PRESCRIPTION":
		sqlRequete += " BEP_PRESCRIPTION WHERE NUMPAT = ";
		if len(tab) != 3{return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MEDICAMENT attendu")}else {
			bl1, err := path.Match("[1-9]*", tab[1])
			if err != nil {
				return "", errors.New("Erreur, NUMERO PATIENT invalide.")
			}
			bl2, err := path.Match("[1-9]*", tab[2])
			if err != nil {
				return "", errors.New("Erreur, NUMERO MEDICAMENT invalide.")
			}
			if bl1 && bl2 {
				sqlRequete += tab[1] + " and NUMMEDICAMENT = " +tab[2]
			} else {
				return "", errors.New("Erreur : NUMERO PATIENT et NUMERO MEDICAMENT attendu")
			}
		}
		break;
	default:
		return "", errors.New("Argument 2, Données invalide (PATIENT, MALADIE, MEDICAMENT, MALADIEPATIENT, PRESCRIPTION)")
	}

	_, err := executeRequete(sqlRequete)
	if err != nil {
		return "", errors.New("Une erreur serveur est survenue lors de l'éxécution de la requete : "+err.Error())
	} else {
		return "SUCCES", nil;
	}
}
